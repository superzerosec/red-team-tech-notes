# Red Team Tech Notes

During our day to day work we are always learning new things or solving random technical problems. As we do this we strive to document and share that knowledge both internally and externally. We will use this repository as a spot to keep our draft notes and make them available for all.

## Index

* [The Mechanics of Modern Thievery (Part 1 of 3)](./mechanics-of-modern-thievery/part-1.md)
* [RT-011 Phishing Campaign - Fake Laptop Upgrade](https://gitlab.com/gitlab-com/gl-security/gl-redteam/red-team-tech-notes/-/tree/master/RT-011%20-%20Phishing%20Campaign)

