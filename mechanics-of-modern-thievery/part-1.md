## The Mechanics of Modern Thievery (Part 1 of 3)

*Rough Draft 1* - 13-May-2020

*Tech note by Greg Johnson @ GitLab*

### Preface

Imagine, for a moment, that you are a career criminal.  A burglar, at that.  You're very good at your job.  It's a quiet morning in the business district of the area you've chosen to case.  You've been watching a target for several days.  It's a large box of a building; a plain, brass, nondescript, standard key is required for entry.  Hundreds if not thousands of employees all coming and going.

You watch as an employee gets out of her car to go into work.  She looks tired, reluctant to enter the building where she'll spend most of her day.  As she closes the door to her car, the key to get into the building slips from her very full hands and falls to the pavement.  She doesn't notice. This is your moment.  

You casually walk toward the dropped key as she begins walking toward the building.  You pick up the key and, quickly, as to avoid notice, produce a small tin box filled with softened putty.  You gently press the key into the putty and imprint its shape, snap the lid closed, and slip the box back into your pocket to later make a mold and copy the key.

"Miss!?", you say, trying to contain your excitement.  "You've dropped your key!"  You kindly hand the dropped key back to her, and walk back to your car across the street.  And just like that, you're in.

#### A Metaphor To Set Up our Topic

The intent of this short story is to draw a good metaphor from which to better explain the mechanics of modern, digital thievery, where we're no longer dealing with physical keys, but digital keys.  Who among us has yet to lose a set of physical keys?  It happens quite often if I'm right.  We'll take a progressively more technical look at digital keys, how they get dropped in the digital world, and how to effectively pick them up before thieves do as we progress through the segments of the article, using our metaphor to better explain the basics.

In recent weeks, the Red Team at GitLab has been busy updating a tool called [gitrob]() to aid our operations.  The tool by [Michael Henriksen](https://github.com/michenriksen) was a perfect stepping stone to get us where we wanted to go.  Open source tooling for scanning GitLab repositories on-demand have been few and far between, so we made our own.

For the purpose of this first post in the series, it's only necessary that you have a basic understanding of [git](https://git-scm.com/), git repositories, and collaboration hosts like GitLab and what they're used for.  We'll have a non-technical introduction in this post with a technical deep dive into the specifics of searching for digital tokens and automating that process for red teaming, penetration testing, and defense strategies in later segments.  

Largely as a response to [a recent blog post I wrote](https://about.gitlab.com/blog/2019/12/20/introducing-token-hunter/) regarding a related tool, Token-Hunter, we'll start with a foundation of what digital keys are, how they are generated, and how they get leaked resulting in impactful data breaches.  In the more technical posts in the series, we'll take a closer look at the open-source tooling available for finding leaked digital keys in the wild and compare them, breaking down the details of how they operate and why.  For now, let's "start from the start".

#### In the Beginning

Keys and locks have taken many forms over the centuries.  Today, physical keys are the most commonly manufactured metal object, enabling us to live in the modern world where almost everything is behind a lock.  They're tiny, easy to carry, and easy to use.  

Keys and locks appeared in human history about 6,000 years ago.  The first simple wooden "pin tumbler" lock used small pins hidden near the bolt along with a wooden key that raised the pins allowing the latch to open.  Though the pin tumbler design is still in heavy use even today, there were some flaws in the original design, otherwise we'd still be using them.  For example, the wooden components were very easy to open with brute force to name one.  Between then and now, we've employed different materials to make keys and locks, changed their shape, design, and even their size.  We've even done away with physical keys completely.

![Egyption Lock](./images/egyption-lock.jpg)



#### What Modern Keys Look Like

In modern times, keys are needed for unlocking more than just your car and house.  Different forms of digital keys are necessary en masse to access your computer, your bank account, your Facebook page, your work email, your personal email(s), and the list goes on.  Software developers, systems administrators, and others responsible for building and maintaining the systems you need access to also need keys for similar, but slightly different needs:  to login to the systems and networks that support the operation of these systems.  

More often than not, these keys take forms other than the traditional username and password combination we're all accustomed to, though those get leaked nearly as often.  Lets take a closer look at an example of what one of these keys may look like:

```json
{
    "AccessKeyID": "AKIABAR4090YTEN91",
    "Secret": "BGrtmN19bR1097135vhjkKGgBM54912yuIPOn456"
}
```

A key like this might provide an administrator [authentication and authorization](https://www.okta.com/identity-101/authentication-vs-authorization/) into one or more systems that control how an application operates in order to fix bugs, adjust configuration, or add resources.  It might allow access to sensitive information needed to maintain those systems:  user accounts, passwords, proprietary data, and servers just for a start.  It may have access to control DNS and domain name registrations which control an organizations presence on the Internet.  It might allow a user to start, stop, and create servers, or any billable resource from a cloud provider depending on its configuration.  Should a key like this fall into the wrong hands, it has the propensity to be extremely detrimental to business in terms of data exposure, resource costs, and time to remediate which could otherwise be spent on other things to move the business forward.  Just like a physical key, it's very difficult to tell precisely what doors it will open without trying a few of them to see for yourself.  Should you come across a leaked key, it will often involve some ingenuity and loads of caution to determine what precisely it has access to.

There are many types of files containing these keys that might be present on a developers' or administrators' machine, and they're often associated and stored with application software, code, or other similar assets.  These configuration files are often shared with teams in collaboration host repositories like GitLab, some of which are public for the world to see like is often the case with [GitLab's own repositories](https://about.gitlab.com/handbook/values/#transparency).  Referring back to our metaphor, perhaps it's easier to think about these repositories that GitLab and others host as if it were the parking lot where our poor employee dropped their physical key.  As, like them, these repositories are often where a key like the one we looked at above get dropped and, subsequently, picked up by a thief.

### Where Modern Keys Come From

In short, and hopefully without getting too technical, modern keys are created by software that generates some random text that has cryptographic significance.  Regardless of whether or not they are generated and issued by Google, GitLab, GitHub, or others, they typically take similar but slightly different textual form.  Let's look at a few more examples of what these keys look like.  

These examples sometimes include large sets of text, so I've stored them in [GitLab snippets](https://docs.gitlab.com/ee/api/project_snippets.html) such that you can delve into each to get a better idea of what these keys look like.  Later in the series, this will be more important as we dive into the technical details of how to detect them in git repositories and [other places](https://about.gitlab.com/blog/2019/12/20/introducing-token-hunter/).  For now, suffice it to say that each of these examples are in many ways similar but also noticeably different:

* [SSH Key Example](https://gitlab.com/gitlab-com/gl-security/gl-redteam/red-team-tech-notes/snippets/1976051)
* [GitLab Personal Access Key Example](https://gitlab.com/gitlab-com/gl-security/gl-redteam/red-team-tech-notes/snippets/1976052)
* [Twitter Access Key Example](https://gitlab.com/gitlab-com/gl-security/gl-redteam/red-team-tech-notes/snippets/1976053)
* [PGP Key Example](https://gitlab.com/gitlab-com/gl-security/gl-redteam/red-team-tech-notes/snippets/1976055)

### How Modern Keys Get Dropped

Modern keys get dropped as a result of human error.  Back to our metaphor, we just have too many things in our proverbial hands and mistakes inevitably are made.  As a [red teamer](https://about.gitlab.com/handbook/engineering/security/red-team/) at GitLab, I often build, modify and automate software tooling to aid our operations using GitLab to store the source code and make sure it compiles and deploys properly.  Recently, I modified [gitrob](https://github.com/codeEmitter/gitrob) to add support for GitLab and add a few new features for secrets detection and performance.  I too keep these types of digital keys on my machine.  Sometimes, I too make mistakes.

As I automated the process that made gitrob work to aid an operation, I made a mistake like many people do.  I committed a change to an internal git repository and accidentally left a sensitive token in a one-off script during that commit.  Granted, this key was leaked on an internal GitLab repository, and it was associated with a counterfeit account that I had created during the operation for read-only access.  Luckily, I'd taken the time to scope it properly.  In other words, when I asked GitLab to generate the key for me, I had specified the principle of least privilege.  In this case, all I needed was read access.  Simply put, I didn't need to change any information on GitLab with the key, so I didn't give the key that level of access.

Regardless, this is how keys get "dropped" in the modern world.  I'm the employee in the parking lot that had dropped their key, drawing from our metaphor.  However, I got lucky, and it was my teammates that discovered the mistake and not a legitimate attacker.  It was easy to remediate, which we'll also go into more detail on later, but it served as a stark reminder to me and my teammates how easily it can happen.  It's ironic, and funny, and the impact in this case was minimal, but that's not always the case.  A simple commit to a repository had exposed my key to a theoretical internal threat.  The stakes are often higher than we like to think with other types of tokens that may have access to more sensitive information as we've discussed previously.

There are other avenues of leakage that happen through exposed application log files that record the happenings within a system, improperly implemented security features within the application, as well a vulnerable implementations of key generation processes.  Despite what you may assume, [computers aren't great at math](https://blog.codinghorror.com/why-do-computers-suck-at-math/).  Generating random sets of numbers and letters involves math and sometimes its possible to directly guess what a computer might consider random.

### The Impact of Leaked Secrets

As easy as it is for engineers to make the same mistake I did and leak sensitive information to public repositories hosted on GitLab, GitHub, Bitbucket, and the like, the results can be absolutely devastating.  A leaked key, a key made public unintentionally, has the very real risk of impacting a business' ability to *stay* in business if they're unable to spend the time and effort to remediate it properly after a breach.  

A leaked key can cause customers to leave due to loss of trust, deadlines to be missed, and inordinate amounts of time to be spent determining the breadth and depth of exposure that could otherwise be spent elsewhere.  The bottom line is that data loss is expensive and human error of the nature we're discussing here is one of the most common vectors by which these types of modern keys get leaked.

### History Repeats Itself

Git repositories are only one vector exposing API tokens and cryptographic keys to theft.  For this article we'll stick close to that paradigm so that we can better understand it.  However, in recent years, [Facebook reset 50 million account passwords](https://arstechnica.com/information-technology/2018/09/50-million-facebook-accounts-breached-by-an-access-token-harvesting-attack/) due to a API token harvesting attack that leaked these same types of modern keys.  GateHub, a popular cryptocurrency wallet website, [had a similar breach](https://gatehub.net/blog/gatehub-preliminary-statement/) that allowed attackers to steal cryptocurrency from users' online wallets.  Amazon Web Services [accidentally leaked a key](https://gizmodo.com/amazon-engineer-leaked-private-encryption-keys-outside-1841160934) very similar to the example above, though it's unclear what the impact could have been due to it being detected within 30 minutes of the leak and reported to AWS two hours later.  

This is best case scenario.  Sometimes these keys will sit in public repositories exposed to the Internet for years before being detected, if they ever are detected.  Why?  Because detection is a hard problem.  Despite many efforts to improve detection capabilities and implement preventive measures, there hasn't been enough effort from the information security community to inform the consumers of these types of modern keys on the basics:  what they look like, how they come into existence, how they commonly get leaked, how to detect them in the wild, and what can be done to remediate a leaked key when, not if, it happens.

### Up Next

In part one of this three part series we established a foundation.  We rendered a metaphor that we will reuse throughout the series to better exemplify the mechanics of modern thievery.  We looked at how modern keys often get exposed, and described briefly some of the work GitLab's Red Team has been doing to support its operations with gitrob. If you were looking for technical specifics on detection, automation, and performance, you're probably sorely disappointed, but fear not!  In part two of this series we'll dive deeply into some of the new features of gitrob, the concepts behind popular techniques used to detect keys, entropy, and more.
